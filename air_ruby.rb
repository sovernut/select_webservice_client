
require 'savon'
require 'pp'

client = Savon::Client.new(wsdl: "http://localhost:3000/airwebservice/wsdl")

#client = Savon::Client.new(wsdl: "http://airwebservice.herokuapp.com/airwebservice/wsdl")
client.operations # => [:integer_to_string, :concat, :add_circle]

#result = client.call(:create_news_shop, message: { :name => "news one"})
#result2 = client.call(:create_news_shop, message: {:name => 'rocket'})

result2 = client.call(:get_all_airinfo)

#result2 = client.call(:add_data,message: {:no => 7,:date_t =>"2018-10-22 10:51:00",:temp => "35.77",:humid =>"77.2"})

#result3 = client.call(:get_shop_name_by_id, message: {:id => 2})
# actual wash_out
#result.to_hash # => {:concat_reponse => {:value=>"123abc"}}
# wash_out below 0.3.0 (and this is malformed response so please update)


#pp result.to_hash
pp result2.to_hash
#pp result3.to_hash